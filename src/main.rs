use std::env;
use std::io::{self, BufRead};
use doe::vec_element_clone;
use regex::Regex;
use std::fs::File;
use std::io::BufReader;

fn main()  -> io::Result<()> {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        eprintln!("USAGE: \n\tkerp <regex pattern> <pipe or input>\n\tkerp <regex pattern> -F <files..>");
        let example = r#"krep '[0-9]{4}-[0-9]{1,}-[0-9]{1,}' -F README.md   =>find date"#;
        eprintln!("Example:\ncat README.md | krep decode\nkrep https -F Cargo.toml README.md   =>find https\n{}",example);
        std::process::exit(1);    
    }
    else if args.len() == 2 {
        let pattern = &args[1];
        let stdin = io::stdin();
        let lines = stdin.lock().lines();
        let re = Regex::new(pattern).unwrap();
        for line in lines {
            let line = line.unwrap();
            if re.is_match(&line) {
                println!("{}", line);
            }
        }
    }    
    else if args.len() > 2 && vec_element_clone!(args,2)== "-F".to_owned(){
        let file_names = &args[3..];
        let pattern = &args[1];
        let re = Regex::new(pattern).unwrap();
        for file_name in file_names {
            let file = File::open(file_name)?;
            let reader = BufReader::new(file);
            for (i, line) in reader.lines().enumerate() {
                let line = line.unwrap();
                if re.is_match(&line) {
                    println!("{}:{}:{}", file_name, i + 1, line);
                }
            }
        }
    }
    Ok(())

}
