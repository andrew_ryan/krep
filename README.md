# Convert excel file to csv file
[![Crates.io](https://img.shields.io/crates/v/loa.svg)](https://crates.io/crates/krep)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/krep)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/krep/-/raw/master/LICENSE)
## install
```sh
cargo install krep
```

```bash
USAGE: 
        kerp <regex pattern> <pipe or input>
        kerp <regex pattern> -F <files..>
Example:
cat README.md | krep decode
krep https -F Cargo.toml README.md   =>find https
krep '[0-9]{4}-[0-9]{1,}-[0-9]{1,}' -F README.md   =>find date
```
